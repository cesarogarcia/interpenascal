import Vue from 'vue'
import Router from 'vue-router'

Vue.use(Router)

const router = new Router({
    routes: [
        {
            path: '/home',
            component: function (resolve) {
                require(['@/components/Home/HomePage.vue'], resolve)
            },
        },

        {
            path: '/product-detail/:id_product',
            component: function (resolve) {
                require([
                    '@/components/ProductInfo/ProductInfoPage.vue',
                ], resolve)
            },
        },
        {
            path: '/add-product',
            component: function (resolve) {
                require([
                    '@/components/AddProduct/FormProductPage.vue',
                ], resolve)
            },
        },
        {
            path: '/productList',
            component: function (resolve) {
                require(['@/components/ProductList/ProductList.vue'], resolve)
            },
        },
        {
            path: '/productList/:categoryName',
            component: function (resolve) {
                require(['@/components/ProductList/ProductList.vue'], resolve)
            },
        },
        {
            path: '/add-user',
            component: function (resolve) {
                require(['@/components/AddUser/AddUserPage.vue'], resolve)
            },
        },
        {
            path: '/userList',
            component: function (resolve) {
                require(['@/components/UserList/UserList.vue'], resolve)
            },
        },
        {
            path: '/user-detail/:id_user',
            component: function (resolve) {
                require(['@/components/UserInfo/UserInfoPage.vue'], resolve)
            },
        },
        {
            path: '/about-us',
            component: function (resolve) {
                require(['@/components/pages/AboutUs.vue'], resolve)
            },
        },
        {
            path: '/privacy-policy',
            component: function (resolve) {
                require(['@/components/pages/PrivacyPolicy.vue'], resolve)
            },
        },
        {
            path: '/productList/searchedProductsByName/:name',
            component: function (resolve) {
                require(['@/components/ProductList/ProductList.vue'], resolve)
            },
        },

    ],
})
export default router
