<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

// ----------------------------------------------------------------Tabla Products----------------------------------------------------------- //

Route::get('/products', function (Request $request) {
    $results = DB::select('select idproduct, name, price, urlphoto from products ');
    return response()->json($results, 200);
});

Route::get('/last-products/products', function (Request $request) {
    $results = DB::select('select idproduct, name, price, urlphoto from products ORDER BY datecreate desc LIMIT 0,(select value_limitation from limitation_last_product)');
    return response()->json($results, 200);
});

Route::get('/search_product/products/{txt}', function ($txt) {
    
    $results = DB::select('select name from products WHERE name LIKE "%' . $txt . '%"');
    return response()->json($results, 200);
});




Route::get('/products/categories/{category}', function ($category) {
    
    if (categoryNoExists($category)) {
        abort(404);
    }
  
    $results = DB::select('select * from products where category=:category', [
        'category' => $category,
    ]);
    return response()->json($results, 200);
});





Route::get('/products/{idproduct}', function ($idproduct) {
   
    if (productNoExists($idproduct)) {
        abort(404);
    }
    $results = DB::select('select * from products where idproduct=:idproduct ', [
        'idproduct' => $idproduct,
    ]);
    return response()->json($results[0], 200);
});







Route::post('/products', function () {
   
    $data = request()->all();
    DB::insert(
        "
        INSERT into products (idproduct, name, description, category, price, urlphoto, tag1, tag2, tag3, datecreate)
        VALUES (:idproduct, :name, :description, :category, :price, :urlphoto, :tag1, :tag2, :tag3, :datecreate)
    ",
        $data
    );
   
    $results = DB::select('select * from products where idproduct = :idproduct', [
        'idproduct' => $data['idproduct'],
    ]);
    return response()->json($results[0], 200);
});
Route::PUT('/products/{idproduct}', function (Request $request, $idproduct) {
     if (productNoExists($idproduct)) {
        abort(404);
    }

  DB::update("update products set  name=?, description=?,  category=?,  price=? ,  urlphoto=?,  tag1=?,  tag2=?,  tag3=?  where idproduct=?",
   [$request->name,
   $request->description,
   $request->category,
   $request->price,
   $request->urlphoto,
   $request->tag1,
   $request->tag2,
   $request->tag3,
   $request->idproduct]);
$results = DB::select('select idproduct, name, description, category, price, urlphoto, tag1, tag2, tag3 from products where idproduct=:idproduct', ['idproduct' => $idproduct]);
return response()->json($results[0], 200);
});
if (!function_exists('categoryNoExists')) {
    function categoryNoExists($category)
    {
        $results = DB::select('select * from products where category=:category', [
            'category' => $category,
        ]);
        return count($results) == 0;
    }
};
if (!function_exists('productNoExists')) {
    function productNoExists($idproduct)
    {
        $results = DB::select('select * from products where idproduct=:idproduct', [
            'idproduct' => $idproduct,
        ]);
        return count($results) == 0;
    }
}
// ---------------------Tabla categories ------------------------------------------ //
Route::get('/categories', function (Request $request) {
    $results = DB::select('select * from categories ORDER BY categoryName');
    return response()->json($results, 200);
});
Route::get('/categories/{idCategory}', function ($idCategory) {
  
    if (productNoExists($idCategory)) {
        abort(404);
    }
   
    $results = DB::select('select * from categories where idCategory=:idCategory ', [
        'idCategory' => $idCategory,
    ]);
  
    return response()->json($results[0], 200);
});
Route::post('/categories', function () {
    
    $data = request()->all();
    DB::insert(
        "
        INSERT into categories (idCategory, categoryName)
        VALUES (:idCategory, :categoryName)
    ",
        $data
    );
    $results = DB::select('select * from categories where idCategory = :idCategory', [
        'idCategory' => $data['idCategory'],
    ]);
    return response()->json($results[0], 200);
});
Route::put('/categories/{idCategory}', function ($idCategory) {
    if (productNoExists($idCategory)) {
        abort(404);
    }
    $data = request()->all();
    DB::delete(
        "
        delete from categories where idCategory = :idCategory",
        ['idCategory' => $idCategory]
    );
    DB::insert(
        "
        INSERT into categories (idCategory, categoryName)
        VALUES (:idCategory, :categoryName)
    ",
        $data
    );
    $results = DB::select('select * from categories where idCategory = :idCategory', ['idCategory' => $idCategory]);
    return response()->json($results[0], 200);
});

if (!function_exists('categoryNoExists')) {
    function categoryNoExists($idCategory)
    {
        $results = DB::select('select * from categories where idCategory=:idCategory', [
            'idCategory' => $idCategory,
        ]);
        return count($results) == 0;
    }
}

// ----------------------------------------------------------------Tabla user----------------------------------------------------------- //
Route::get('/users', function (Request $request) {
    $results = DB::select('select iduser, nickname, mail, role, penascales, urlphoto from users ');
    return response()->json($results, 200);
});
Route::get('/users/roles/{role}', function ($role) {
  
    if (roleNoExists($role)) {
        abort(404);
    }
    
    $results = DB::select('select * from users where role=:role', [
        'role' => $role,
    ]);
    
    return response()->json($results, 200);
});
Route::get('/users/{iduser}', function ($iduser) {
    
    if (userNoExists($iduser)) {
        abort(404);
    }
   
    $results = DB::select('select iduser, nickname, mail, role, penascales, urlphoto from users where iduser=:iduser ', [
        'iduser' => $iduser,
    ]);
  
    return response()->json($results[0], 200);
});
Route::post('/users', function () {
   
    $data = request()->all();
    DB::insert(
        "
        INSERT into users (iduser, nickname, mail, password, datecreate)
        VALUES (:iduser, :nickname,:mail,  :password, :datecreate)
    ",
        $data
    );
   
    $results = DB::select('select * from users where iduser = :iduser', [
        'iduser' => $data['iduser'],
    ]);
    return response()->json($results[0], 200);
});



Route::PUT('/users/{iduser}', function (Request $request, $iduser) {
    if (userNoExists($iduser)) {
        abort(404);
    }


  DB::update("update users set  nickname=?, role=?,  penascales=?,  urlphoto=?     where iduser=?",
   [$request->nickname,
   $request->role,
   $request->penascales,
   $request->urlphoto,
   $request->iduser]);

$results = DB::select('select iduser, nickname, mail, role, penascales, urlphoto from users where iduser=:iduser', ['iduser' => $iduser]);
return response()->json($results[0], 200);
});


if (!function_exists('roleNoExists')) {
    function roleNoExists($role)
    {
        $results = DB::select('select * from users where role=:role', [
            'role' => $role,
        ]);
        return count($results) == 0;
    }
};
if (!function_exists('userNoExists')) {
    function userNoExists($iduser)
    {
        $results = DB::select('select * from users where iduser=:iduser', [
            'iduser' => $iduser,
        ]);
        return count($results) == 0;
    }
}

// ---------------------Tabla roles ------------------------------------------ //
Route::get('/roles', function (Request $request) {
    $results = DB::select('select * from roles');
    return response()->json($results, 200);
});
Route::get('/roles/{idrole}', function ($idrole) {
  
    if (roleNoExists($idrole)) {
        abort(404);
    }
   
    $results = DB::select('select * from roles where idrole=:idrole ', [
        'idrole' => $idrole,
    ]);
  
    return response()->json($results[0], 200);
});
Route::post('/roles', function () {
    
    $data = request()->all();
    DB::insert(
        "
        INSERT into roles (idrole, categoryName)
        VALUES (:idrole, :categoryName)
    ",
        $data
    );
    $results = DB::select('select * from roles where idrole = :idCategoidrolery', [
        'idrole' => $data['idrole'],
    ]);
    return response()->json($results[0], 200);
});
Route::put('/roles/{idrole}', function ($idrole) {
    if (roleNoExists($idrole)) {
        abort(404);
    }
    $data = request()->all();
    DB::delete(
        "
        delete from roles where idrole = :idrole",
        ['idrole' => $idrole]
    );
    DB::insert(
        "
        INSERT into roles (idrole, categoryName)
        VALUES (:idrole, :categoryName)
    ",
        $data
    );
    $results = DB::select('select * from roles where idrole = :idrole', ['idrole' => $idrole]);
    return response()->json($results[0], 200);
});

if (!function_exists('roleNoExists')) {
    function roleNoExists($idrole)
    {
        $results = DB::select('select * from roles where idrole=:idrole', [
            'idrole' => $idrole,
        ]);
        return count($results) == 0;
    }
}
// ----------------------------------------------------------------Tabla xxxxx----------------------------------------------------------- //